//
//  MTModalTweet.m
//  MyTweeter
//
//  Created by Racine, James, Ernest on 3/24/14.
//  Copyright (c) 2014 Racine, James, Ernest. All rights reserved.
//

#import "MTModalTweet.h"

@interface MTModalTweet ()
@property (weak, nonatomic) IBOutlet UITextField *tweetText;
@property (weak, nonatomic) IBOutlet UIButton *tweetButton;

@end

@implementation MTModalTweet

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [_tweetText becomeFirstResponder];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)pushTweet:(id)sender {
    if([self.delegate respondsToSelector:@selector(postTweet:)]){
        [self.delegate postTweet:_tweetText.text];
        [self.delegate closeWindow];
    }
}
- (IBAction)cancelTweet:(id)sender {
    if([self.delegate respondsToSelector:@selector(closeWindow)]){
       [self.delegate closeWindow];
    }
}
- (IBAction)checkTweetLength:(id)sender {
    if([[_tweetText text] length] > 140){
        [_tweetButton setEnabled:NO];
    }else{
        [_tweetButton setEnabled:YES];
    }
}

@end
