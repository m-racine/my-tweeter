//
//  MTPersonListViewController.m
//  MyTweeter
//
//  Created by Racine, James, Ernest on 2/24/14.
//  Copyright (c) 2014 Racine, James, Ernest. All rights reserved.
//

#import "MTPersonListViewController.h"
#import "MTTweetListViewController.h"

@interface MTPersonListViewController ()

@end

@implementation MTPersonListViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    UITabBarItem *item;
    item = [[UITabBarItem alloc] initWithTabBarSystemItem:UITabBarSystemItemContacts tag:0];
    self.tabBarItem = item;
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)viewSonic:(id)sender {
    MTTweetListViewController *tweet;
    tweet = [[MTTweetListViewController alloc] initWithNibName:@"MTTweetListViewController" person:@"Sonic"];

    [self.navigationController pushViewController:tweet animated:YES];
}
- (IBAction)viewDarunia:(id)sender {
    MTTweetListViewController *tweet;
    tweet = [[MTTweetListViewController alloc] initWithNibName:@"MTTweetListViewController" person:@"Darunia"];
    
    [self.navigationController pushViewController:tweet animated:YES];
}

@end
