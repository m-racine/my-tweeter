//
//  MTPlacemark.h
//  MyTweeter
//
//  Created by Racine, James, Ernest on 3/26/14.
//  Copyright (c) 2014 Racine, James, Ernest. All rights reserved.
//

#import <MapKit/MapKit.h>
#import "MTPerson.h"

@interface MTPlacemark : MKPlacemark

@property MTPerson *user;

-(id)initWithPlacemark:(CLPlacemark *)placemark User:(MTPerson*)user;

@end
