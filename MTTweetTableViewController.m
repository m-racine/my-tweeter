//
//  MTTweetTableViewController.m
//  MyTweeter
//
//  Created by Racine, James, Ernest on 3/3/14.
//  Copyright (c) 2014 Racine, James, Ernest. All rights reserved.
//

#import "MTTweetTableViewController.h"
#import "MTAppDelegate.h"
#import "MTPerson.h"

@interface MTTweetTableViewController ()

@end

@implementation MTTweetTableViewController

- (id) initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil style:(UITableViewStyle) tableStyle tweetList:(NSArray *)tweets{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if(self){
        _tweetList = [[NSArray alloc] initWithArray:tweets];
        [self.tableView registerNib:[UINib nibWithNibName:@"TweetCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"Tweet"];
    }
    return self;
}

- (id) initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil style:(UITableViewStyle) tableStyle tweetList:(NSArray *)tweets user:(MTPerson *)p{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if(self){
        _tweetList = [[NSArray alloc] initWithArray:tweets];
        [self.tableView registerNib:[UINib nibWithNibName:@"TweetCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"Tweet"];
        _user = p;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    UIBarButtonItem *refreshButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh target:self action:@selector(refreshTable)];
    self.navigationItem.rightBarButtonItem = refreshButton;
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *tweet;   // assume already initialized to hold tweet
    tweet = [[NSString alloc] initWithString:[[_tweetList objectAtIndex:indexPath.row] text]];
    NSRange r;
    NSString *regEx = @"http://\\S*";
    r = [tweet rangeOfString:regEx options:NSRegularExpressionSearch];
    if (r.location != NSNotFound) {
        // URL was found
        NSString *url = [tweet substringWithRange:r];
        UIWebView *web = [[UIWebView alloc] init];
        [web loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:url]]];
        UIViewController *vc = [[UIViewController alloc] init];
        [vc setView:web];
        [self.navigationController pushViewController:vc animated:YES];
    } else {
        // no URL was found
    }
    
}

-(void)refreshTable{
    UIActivityIndicatorView *activityView=[[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    activityView.center=self.view.center;
    [self.view addSubview:activityView];
    //[activityView setHidesWhenStopped:NO];
    [activityView startAnimating];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(){
        MTAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
        [appDelegate refreshTweetsForTable:self];
        [activityView stopAnimating];
    });
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    //#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //#warning Incomplete method implementation.
    // Return the number of rows in the section.
    return [_tweetList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Tweet";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    // Configure the cell...
    //cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    cell = [[UITableViewCell alloc] init];
    cell = [[[NSBundle mainBundle] loadNibNamed:@"TweetCell" owner:self options:nil] objectAtIndex:0];
    NSDictionary *item = (NSDictionary *)[self.tweetList objectAtIndex:indexPath.row];
    UILabel* label;
    label = (UILabel*)[cell viewWithTag:1];
    label.text = [item valueForKey:@"text"];
    label = (UILabel*)[cell viewWithTag:2];
    label.text = [item valueForKey:@"date"];
    UIImageView* image;
    image = (UIImageView*)[cell viewWithTag:3];
    NSURL *url = [NSURL URLWithString:[item valueForKey:@"userPhoto"]];
    NSData *data = [NSData dataWithContentsOfURL:url];
    image.image = [[UIImage alloc] initWithData:data];
    return cell;
}

/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
 {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 }
 else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

/*
 #pragma mark - Navigation
 
 // In a story board-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 
 */


@end
