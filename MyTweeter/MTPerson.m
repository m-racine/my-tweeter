//
//  MTPerson.m
//  MyTweeter
//
//  Created by Racine, James, Ernest on 2/26/14.
//  Copyright (c) 2014 Racine, James, Ernest. All rights reserved.
//

#import "MTPerson.h"

@implementation MTPerson

-(id)initWithUser:(NSString *)u Photo:(NSString *)p SN:(NSString *)sn{
    if(self = [super init]){
        _user = u;
        _photo = p;
        _handle = sn;
        _tweets = [[NSMutableArray alloc] init];
    }
    return self;
}

-(id)initWithUser:(NSString *)u Photo:(NSString *)p SN:(NSString *)sn location:(MKPlacemark*)loc{
    if(self = [super init]){
        _user = u;
        _photo = p;
        _handle = sn;
        _location = loc;
        _tweets = [[NSMutableArray alloc] init];
    }
    return self;
}

-(void)addTweet:(MTTweet *)tweet{
    [_tweets addObject:tweet];
    return;
}

@end
