//
//  MTTweet.h
//  MyTweeter
//
//  Created by Racine, James, Ernest on 2/26/14.
//  Copyright (c) 2014 Racine, James, Ernest. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MTTweet : NSObject

@property (strong, nonatomic) NSString *userName;
@property (strong, nonatomic) NSString *userPhoto;
@property (strong, nonatomic) NSString *text;
@property (strong, nonatomic) NSString *date;
@property (strong,nonatomic)  NSString *id_string;

-(id) initWithUser:(NSString*)user Text:(NSString*)text Date:(NSString*)date ID:(NSString*)i;

@end
