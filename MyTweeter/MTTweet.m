//
//  MTTweet.m
//  MyTweeter
//
//  Created by Racine, James, Ernest on 2/26/14.
//  Copyright (c) 2014 Racine, James, Ernest. All rights reserved.
//

#import "MTTweet.h"
#import "MTPerson.h"

@implementation MTTweet

-(id)initWithUser:(MTPerson*)user Text:(NSString*)text Date:(NSString*)date ID:(NSString*)i{
    if(self = [super init]){
        _userName = user.user;
        _userPhoto = user.photo;
        _text = text;
        _date = date;
        _id_string = i;
    }
    return self;
}

@end
