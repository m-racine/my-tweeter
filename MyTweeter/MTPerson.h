//
//  MTPerson.h
//  MyTweeter
//
//  Created by Racine, James, Ernest on 2/26/14.
//  Copyright (c) 2014 Racine, James, Ernest. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MTTweet.h"
#import <MapKit/MKPlacemark.h>


@interface MTPerson : NSObject

@property (strong, nonatomic) NSString *user;
@property (strong, nonatomic) NSString *photo;
@property (strong, nonatomic) NSString *handle;
@property (strong, nonatomic) NSMutableArray *tweets;
@property (strong, nonatomic) MKPlacemark *location;

-(id)initWithUser:(NSString*)u Photo:(NSString*)p SN:(NSString*)sn;

-(void)addTweet:(MTTweet*)tweet;

@end
